from firedrake import *
import numpy as np
from ops import *

class PrimalPoissonSignorini(ObstacleProblem):

    def __init__(self, N, k):
        self.N = N
        self.k = k
        self.boundary_label = 3

        self.mesh = self.Mesh()
        self.V = self.primal_function_space(self.mesh)
        self.M = self.dual_function_space(self.mesh)
        self.nsp = None

        self.dofM_contact = self.contact_indices(self.M)
        self.g = self.obstacle(self.mesh)

        self.outfile = File("output/u.pvd")

    def rhs(self):
        return Constant(-1)

    def obstacle(self, mesh):
        x = SpatialCoordinate(mesh)
        x0 = Constant((0.5, 0.0))
        sigma = 0.1
        g = 0.2 * exp ( -dot(x - x0,x - x0)/sigma) - 0.1
        return g

    def primal_function_space(self, mesh):
        return FunctionSpace(mesh, "CG", k)

    def dual_function_space(self, mesh):
        return FunctionSpace(mesh, "CG", k)

    def Mesh(self):
        return UnitSquareMesh(self.N,self.N)

    def residual(self, u, lamda, v):
        f = self.rhs()
        F = (
            inner( grad(u), grad(v)) * dx
            - lamda * v * ds(self.boundary_label)
            - inner(f, v) * dx
        )
        return F

    def solver_parameters(self):
        params = {
                 "ksp_max_it": 30000,
                 "ksp_converged_reason": None,
                 "snes_monitor": None,
                 "ksp_monitor_true_residual": None,
                 "ksp_rtol": 1.0e-12,
                 "ksp_atol": 1.0e-12,
                 "snes_max_it": 1,
                 "snes_rtol": 1.0,
                 # "ksp_type": "fgmres"
                 "ksp_type" : "preonly",
                 "pc_type" : "lu",
                 "mat_type" : "aij",
                 "pc_factor_mat_solver_type": "mumps",
                 "mat_mumps_icntl_14": 200,
                 }
        return params

    def boundary_conditions(self, V):
        return DirichletBC(V, Constant((0)), (1,2,4))

    def primal_solver(self):
        bcs = self.boundary_conditions(self.V)
        self.u = Function(self.V)
        self.lamda = Function(self.M)
        v = TestFunction(self.V)
        F = self.residual(self.u, self.lamda, v)
        nvp = NonlinearVariationalProblem(F, self.u, bcs=bcs)
        nvs = NonlinearVariationalSolver(nvp, solver_parameters=self.solver_parameters(), nullspace=self.nsp)
        return nvs

    def dual_solver(self):
        self.Z = MixedFunctionSpace([self.V, self.M])
        bc_V = self.boundary_conditions(self.Z.sub(0))
        bc_M_complement = self.dual_complement_bc(self.Z.sub(1))
        bcs = [bc_V, bc_M_complement]

        self.u = Function(self.V)
        self.lamda = Function(self.M)
        self.z = Function(self.Z)
        (u, lamda) = split(self.z)
        (v, mu) = split(TestFunction(self.Z))

        # Define auxiliary functions
        self.lamda_old = Function(self.M)
        self.cutoff_A = Function(self.M)

        F = self.residual(u, lamda, v)
        F += self.cutoff_A * (u - self.g) * mu * ds(self.boundary_label)
        F += (1 - self.cutoff_A) * lamda * mu * ds(self.boundary_label)

        nvp = NonlinearVariationalProblem(F, self.z, bcs=bcs)
        nvs = NonlinearVariationalSolver(nvp, solver_parameters=self.solver_parameters(), nullspace=self.nsp)
        return nvs

    def update_cutoff_fcns(self, A_contact):
        A_M = np.array(problem.dofM_contact)[A_contact]
        ind_M = np.zeros(self.M.dim())
        ind_M[A_M] = np.ones(len(A_M))
        self.cutoff_A.dat.data[:] = ind_M

    def dual_update(self):
        u, lamda = self.z.split()
        self.u.assign(u)
        self.lamda.assign(lamda)

    def dual_complement_bc(self, M):
        nodes_function_space = list(range(M.dim()))
        nodes_complement = list(set(nodes_function_space) - set(self.dofM_contact))
        return DirichletBCDoFs(nodes_complement, M, Constant(0))

    def contact_indices(self, V):
        return DoFIndex(V, self.boundary_label)

    def complementarity(self, c):
        uM = Function(self.M)
        uM.interpolate(self.u-self.g)
        F = self.lamda.dat.data[self.dofM_contact] - c*uM.dat.data[self.dofM_contact]
        return F

    def construct_lamda_new(self, F):
        lamda_new = Function(self.M)
        F[F < 0] = 0
        lamda_new.dat.data[self.dofM_contact] = F
        return lamda_new

    def dual_error(self, mu):
        diff_squared = assemble((self.lamda - mu)**2 * ds(self.boundary_label) )
        return np.sqrt(diff_squared)

    def update(self, mu):
        self.lamda.assign(mu)

    def save_file(self):
        self.outfile.write(self.u)


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--solver", type = str, required = True, choices = ["uzawa", "pdas"])
    parser.add_argument("--N", type = int, required = True)
    parser.add_argument("--k", type = int, default = 1)
    args, _ = parser.parse_known_args()

    N = args.N
    k = args.k

    tol = 1e-6
    c = 1
    k_max = 200

    problem = PrimalPoissonSignorini(N, k)

    if args.solver == "uzawa":
        iter = UzawaSolver(problem, tol, c, k_max)
    elif args.solver == "pdas":
        iter = PrimalDualActiveSet(problem, tol, c, k_max)

    u = problem.u
    lamda = problem.lamda
    # Check precision of obstacle
    V = u.function_space()
    M = lamda.function_space()
    mesh = V.mesh()
    g = interpolate(problem.obstacle(mesh), V)
    print("u(0.5,0) :: %.4e" % u([0.5,0.0]))
    print("g(0.5,0) :: %.4e" % g([0.5,0.0]))

    n = FacetNormal(mesh)
    udn = dot(grad(u), n)

    # Check precision of obstacle
    def plus(v):
        return 0.5*(abs(v) + v)
    penalty = (plus(g-u))**2 * ds(3)
    penalty_dual = (plus(-udn))**2 * ds(3)
    complementarity = (udn * (u - g))**2 * ds(3)

    print("PENALTY |g-u| :: %.2e \nPENALTY |partial_n u| :: %.2e \nCOMPLEMENTARITY :: %.2e"
            % (assemble(penalty), assemble(penalty_dual), assemble(complementarity)) )


    # Plot
    import matplotlib.pylab as plt
    colors = ["red", "blue", "green"]
    def plotandsave(x,y,file, legend = None):
        fig = plt.figure()
        for i in range(len(x)): plt.plot(x[i], y[i], color = colors[i])
        if legend is not None: plt.legend(legend)
        plt.savefig(file)
        plt.close(fig)

    # Plot lamda
    xM_sort, dofM_sort = SortedXDoF(M, problem.boundary_label)
    plotandsave([xM_sort], [lamda.dat.data[dofM_sort]], "output/lamda.png")

    # Plot obstacle
    xV_sort, dofV_sort = SortedXDoF(V, problem.boundary_label)
    plotandsave([xV_sort, xV_sort], [u.dat.data[dofV_sort], g.dat.data[dofV_sort]], "output/u.png", legend = ["u", "obstacle"])
