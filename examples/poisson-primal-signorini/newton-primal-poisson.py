from firedrake import *
import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("--N", type = int, required = True)
parser.add_argument("--k", type = int, default = 1)
args, _ = parser.parse_known_args()
N = args.N
k = args.k
c = Constant(1e-5)

mesh = UnitSquareMesh(N,N, quadrilateral=True)
V = FunctionSpace(mesh, "CG", k)
M = FunctionSpace(mesh, "CG", k)
Z = MixedFunctionSpace([V, M])
z = Function(Z)
(u, lamda) = split(z)
(v, mu) = split(TestFunction(Z))

x = SpatialCoordinate(mesh)
x0 = Constant((0.5, 0.0))
sigma = 0.1
f = Constant(-1)
g = 0.2 * exp ( -dot(x - x0,x - x0)/sigma) - 0.1

class InvertedDirichletBC(DirichletBC):
    @utils.cached_property
    def nodes(self):
        nodes_on_boundary = self._function_space.boundary_nodes(self.sub_domain, self.method)
        nodes_function_space = list(range(self._function_space.dim()))
        nodes_complement = np.array(list(set(nodes_function_space) - set(nodes_on_boundary)), dtype=nodes_on_boundary.dtype)
        return nodes_complement

bcs = [DirichletBC(Z.sub(0), Constant(0), (1,2,4)), InvertedDirichletBC(Z.sub(1), Constant(0), 3, method="geometric")]

def plus(x):
    return conditional(ge(x, 0), x, 0)

complementarity = plus(lamda - c * (u-g))

F = (
    inner( grad(u), grad(v)) * dx
    - inner(lamda, v) * ds(3)
    - inner(f, v) * dx
    + inner(lamda, mu) * ds(3)
    - inner(complementarity, mu) * ds(3)
)

problem = NonlinearVariationalProblem(F, z, bcs = bcs)
params = {
         "snes_monitor": None,
         "snes_linesearch_type": "l2",
         "snes_max_it": 100,
         "snes_rtol": 1.0e-14,
         "snes_atol": 1.0e-14,
         "snes_stol": 0,
         "snes_converged_reason": None,
         "snes_linesearch_monitor": None,
         "ksp_type": "preonly",
         # "snes_atol": 1.0e-10,
         # "snes_rtol": 1.0e-10,
         # # "snes_stol": 0.0, # worst default in PETSc
         # "snes_max_linear_solve_fail": 150,
         # "snes_monitor": None,
         # "snes_converged_reason": None,
         # "snes_linesearch_type": "l2",
         # "snes_linesearch_monitor": None,
         # "snes_linesearch_maxstep": 1.0,
         # "snes_linesearch_damping": 1,
         # "snes_linesearch_monitor": None,
         # "ksp_max_it": 30,
         # "ksp_converged_reason": None,
         "ksp_monitor": None,
         # "ksp_type" : "preonly",
         "pc_type" : "lu",
         "pc_svd_monitor" : None,
         "mat_type" : "aij",
         "pc_factor_mat_solver_type": "mumps",
         "mat_mumps_icntl_14": 200,
         }
solver = NonlinearVariationalSolver(problem, solver_parameters = params)
solver.solve()

u = z.split()[0]
lamda = z.split()[1]
n = FacetNormal(mesh)
udn = dot(grad(u), n)

# Check precision of obstacle
penalty = (plus(g-u))**2 * ds(3)
penalty_dual = (plus(-udn))**2 * ds(3)
complementarity = (udn * (u - g))**2 * ds(3)

print("PENALTY |g-u| :: %.2e \nPENALTY |partial_n u| :: %.2e \nCOMPLEMENTARITY :: %.2e"
        % (assemble(penalty), assemble(penalty_dual), assemble(complementarity)) )

V_scalar = FunctionSpace(mesh, "CG", 1)
File("output/g.pvd").write(interpolate(g, V_scalar))
File("output/u_primal.pvd").write(interpolate(u, V_scalar))
File("output/lamda.pvd").write(interpolate(lamda, V_scalar))
print("u(0.5,0) :: %.4e" % u([0.5,0.0]))
print("g(0.5,0) :: %.4e" % g([0.5,0.0]))



###
from ops import *

# Plot
import matplotlib.pylab as plt
colors = ["red", "blue", "green"]
def plotandsave(x,y,file, legend = None):
    fig = plt.figure()
    for i in range(len(x)): plt.plot(x[i], y[i], color = colors[i])
    if legend is not None: plt.legend(legend)
    plt.savefig(file)
    plt.close(fig)

# Plot lamda
xM_sort, dofM_sort = SortedXDoF(M, 3)
plotandsave([xM_sort], [lamda.dat.data[dofM_sort]], "output/lamda.png")
print(lamda.dat.data[dofM_sort])

# Plot obstacle
g_V = interpolate(g, V)
xV_sort, dofV_sort = SortedXDoF(V, 3)
plotandsave([xV_sort, xV_sort], [u.dat.data[dofV_sort], g_V.dat.data[dofV_sort]], "output/u.png", legend = ["u", "obstacle"])
print(u.dat.data[dofV_sort] - g_V.dat.data[dofV_sort])
