from firedrake import *
import numpy as np
from ops import *

def ContactCoordsMesh(mesh, boundary_label):
    U = mesh.coordinates.function_space()
    ele = mesh.coordinates.function_space().ufl_element()
    if U.finat_element.entity_dofs() == U.finat_element.entity_closure_dofs():
        CGkele = ele.reconstruct(family="Lagrange")
        CGk = FunctionSpace(mesh.coordinates.function_space().mesh(), CGkele)
        coordinates = interpolate(mesh.coordinates, CGk)#, access=op2.MAX)
        xend, xstart = min(mesh.coordinates.dat.data[:,0]), max(mesh.coordinates.dat.data[:,0])
        periodic = True
    else:
        coordinates = mesh.coordinates
        periodic = False

    U = coordinates.function_space()
    dofs_coords_bndry = DoFIndex(U, boundary_label)
    coords_bndry = coordinates.dat.data[dofs_coords_bndry, :]

    # Either the starting point or the end point is missing in coords_bndry
    x_bndry = coords_bndry[:,0]

    if periodic:
        x_bndry = np.append(x_bndry, [xend, xstart])
        seen = set()
        x_bndry = [x for x in x_bndry if not (x in seen or seen.add(x))]
    x_bndry_sorted = np.sort(x_bndry)

    return coords_bndry, x_bndry_sorted

def ContactMesh(coords_bndry, x_bndry_sorted):
    # arbitrary Length with same number of elements
    Nbm = coords_bndry.shape[0]
    BndMesh = PeriodicIntervalMesh(Nbm, 4)
    xbm = BndMesh.coordinates.dat.data
    _,ind = np.unique(xbm, return_inverse = True) # indices of ordering
    for (i, ind_i) in enumerate(ind):
        xbm[i] = x_bndry_sorted[ind_i]
    return BndMesh

def ContactFunctionSpace(V, contact_mesh):
    ele = V.ufl_element().family()
    k = V.ufl_element().degree()
    return FunctionSpace(contact_mesh, ele, k)

def SortedIndices(U, dofU_label = None):
    dofU_coords = DoFCoords(U)
    if dofU_label is not None:
        x_dofU_label = dofU_coords.dat.data[dofU_label, 0]
    else:
        x_dofU_label = dofU_coords.dat.data ## I assume this is the 1D mesh
    ind_sort_U = np.argsort(x_dofU_label)
    return ind_sort_U, x_dofU_label
