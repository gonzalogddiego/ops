from firedrake import *
from alig import *
import numpy as np
from datetime import datetime

class SubGlacialCavity(NavierStokesProblem):
    def __init__(self, baseN, lamda, H, a, utop):
        super().__init__()
        self.baseN = baseN
        self.lamda = lamda
        self.H = H
        self.a = a
        self.utop = utop

    # mesh stretching Function
    def stretch(self, z, H):
        return 1-z/float(H)

    def BedGeometry(self, x, a, lamda):
        return -a/2.*(1-np.sin(0.5*np.pi - 2*np.pi/lamda*x))

    def mesh(self, distribution_parameters):
        base = CylinderMesh(self.baseN, self.baseN, 1.0, 1.0,
                             distribution_parameters=distribution_parameters)
        self.Deform(base)
        return base

    def Deform(self, mesh):
        coords = mesh._plex.getCoordinatesLocal().array.reshape(-1, mesh.geometric_dimension())
        s = np.arctan2(coords[:,1], coords[:,0])
        s = (2*np.pi + s) * (s < 0) + s*(s > 0)
        L = 2*np.pi
        coords[:,2] += self.stretch(coords[:,2], 1)*self.BedGeometry(s, self.a, L)

    def bcs(self, Z):
        bcs = [DirichletBC(Z.sub(0).sub(0), Constant(self.utop), 2)]
        return bcs

    def has_nullspace(self): return False

    def relaxation_direction(self): return "0+:1-"

    def mesh_hierarchy(self, hierarchy, nref, callbacks, distribution_parameters):
        baseMesh = self.mesh(distribution_parameters)
        if hierarchy == "bary":
            mh = BaryMeshHierarchy(baseMesh, nref, callbacks=callbacks,
                                   reorder=True, distribution_parameters=distribution_parameters)
        elif hierarchy == "uniformbary":
            bmesh = Mesh(bary(baseMesh._plex), distribution_parameters={"partition": False})
            mh = MeshHierarchy(bmesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        elif hierarchy == "uniform":
            mh = MeshHierarchy(baseMesh, nref, reorder=True, callbacks=callbacks,
                               distribution_parameters=distribution_parameters)
        else:
            raise NotImplementedError("Only know bary, uniformbary and uniform for the hierarchy.")

        meshes = tuple(periodise(m, self.lamda, self.H) for m in mh)
        mh = HierarchyBase(meshes, mh.coarse_to_fine_cells, mh.fine_to_coarse_cells)
        return mh
