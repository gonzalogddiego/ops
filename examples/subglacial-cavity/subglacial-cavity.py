from firedrake import *
import numpy as np
from ops import *
from alig import *
from alig_problem import SubGlacialCavity as SubGlacialCavityAliG
from other_functions import *

class SubGlacialCavity(ObstacleProblem):

    def __init__(self, N, L, H, amp, utop,
                A_val, ng_val, eps_min_val, args):

        self.k = args.k
        self.N = N
        self.L = L
        self.H = H
        self.amp = amp

        # Initialise problem
        self.problem = SubGlacialCavityAliG(args.baseN, L, H, amp, utop)
        self.solver_alig = get_solver(args, self.problem)
        self.solver_alig.A_val.assign(A_val)
        self.solver_alig.ng_val.assign(ng_val)
        self.solver_alig.eps_min_val.assign(eps_min_val)

        self.mesh = self.solver_alig.mesh
        self.Z = self.solver_alig.function_space(self.mesh, self.k)
        self.M = self.dual_function_space(self.mesh)
        self.bcs = self.problem.bcs(self.Z)

        self.z = self.solver_alig.z
        self.lamda = Function(self.M)

        self.dofV_contact = self.contact_indices(self.Z.sub(0))
        self.dofM_contact = self.contact_indices(self.M)

        self.regularisation = Constant(min(self.mesh.cell_sizes.dat.data))**0.5

        # Define boundary mesh and associated spaces
        self.coords_bndry, x_bndry_sorted = ContactCoordsMesh(self.mesh, 1)
        self.contact_mesh = ContactMesh(self.coords_bndry, x_bndry_sorted)
        Vc = ContactFunctionSpace(self.Z.sub(0), self.contact_mesh)
        Mc = ContactFunctionSpace(self.M, self.contact_mesh)

        # Initialise neccessary functions in contact_mesh
        self.uc_x = Function(Vc)
        self.uc_y = Function(Vc)
        self.un_c = Function(Mc)
        self.un = Function(self.M)

        # Obtain boundary p.w. linear function for height of cavity
        self.theta = self.cavity_height()

        # Necessary elements for mapping from domain to boundary and vice versa
        self.ind_sort_V, x_dof_V = SortedIndices(self.Z.sub(0), self.dofV_contact)
        self.ind_sort_Vc, x_dof_Vc = SortedIndices(Vc)
        self.ind_sort_M, x_dof_M = SortedIndices(self.M, self.dofM_contact)
        self.ind_sort_Mc, x_dof_Mc = SortedIndices(Mc)

        # Make sure both either start at 0 or finish at 1!
        self.assert_ordering(x_dof_V, self.ind_sort_V, x_dof_Vc, self.ind_sort_Vc, "V")
        self.assert_ordering(x_dof_M, self.ind_sort_M, x_dof_Mc, self.ind_sort_Mc, "M")

        # Write outfiles
        self.File_u = File("output/u.pvd")
        self.File_p = File("output/p.pvd")
        self.uplot = Function(self.Z.sub(0))
        self.pplot = Function(self.Z.sub(1))

    def dual_function_space(self, mesh):
        return FunctionSpace(mesh, "CG", 1)

    def residual(self):
        assert self.solver_alig.num_variables == 3, "Only implemented for 2 field"
        (u, p) = split(self.z)
        (v, q) = split(TestFunction(self.Z))

        F = self.solver_alig.F
        n = FacetNormal(self.mesh)

        F -= -self.N * dot(v , n) * ds(2)
        F -= self.lamda  * dot(v , n) * ds(1)
        F += self.regularisation * inner(u,v) * dx

        return F

    def primal_solver(self):
        F = self.residual()
        sp = self.solver_alig.get_parameters()

        # Quotient out the rigid body modes
        MVSB = MixedVectorSpaceBasis
        x, y = SpatialCoordinate(self.mesh)
        b1 = Function(self.Z.sub(0))
        b2 = Function(self.Z.sub(0))
        b3 = Function(self.Z.sub(0))
        b1.interpolate(Constant([1, 0]))
        b2.interpolate(Constant([0, 1]))
        b3.interpolate(as_vector([-y, x]))
        nullmodes = VectorSpaceBasis([b1, b2, b3])
        nullmodes.orthonormalize()
        nsp = MVSB(self.Z, [nullmodes, self.Z.sub(1)])
        # nsp = None

        nvp = NonlinearVariationalProblem(F, self.z, bcs=self.bcs)
        nvs = NonlinearVariationalSolver(nvp, solver_parameters=sp, nullspace=nsp,
                                        appctx = self.solver_alig.appctx)
        nvs.set_transfer_manager(self.solver_alig.transfermanager)
        return nvs

    def contact_indices(self, V):
        return DoFIndex(V, 1)

    def complementarity(self, c):
        self.uNormal()
        F = self.lamda.dat.data[self.dofM_contact] - c*self.un.dat.data[self.dofM_contact]
        return F

    def construct_lamda_new(self, F):
        lamda_new = Function(self.M)
        F[F > 0] = 0
        lamda_new.dat.data[self.dofM_contact] = F
        return lamda_new

    def dual_error(self, mu):
        diff_squared = assemble((self.lamda - mu)**2 * ds(1))
        return np.sqrt(diff_squared)

    def update(self, mu):
        self.lamda.assign(mu)

    def uNormal(self):

        u = self.z.split()[0]

        # Obtain x and y components of velocity as Vc function
        for (i,ind) in enumerate(self.ind_sort_V):
            ind_c = self.ind_sort_Vc[i]
            ui = u.dat.data[self.dofV_contact[ind], :]
            self.uc_x.dat.data[ind_c] = ui[0]
            self.uc_y.dat.data[ind_c] = ui[1]

        # Project ux * theta.dx(0) - uy onto Mc
        self.un_c.interpolate(self.uc_x * self.theta.dx(0) - self.uc_y)

        # Obtain function un in M with values from Mc function on contact region
        for (i, ind) in enumerate(self.ind_sort_M):
            self.un.dat.data[self.dofM_contact[ind]] = self.un_c.dat.data[self.ind_sort_Mc[i]]


    def cavity_height(self):
        Wc = FunctionSpace(self.contact_mesh, "CG", 1)
        theta = Function(Wc)
        ind_sort_Wc, x_dof_Wc = SortedIndices(Wc)
        ind_sort_mesh = np.argsort(self.coords_bndry[:,0])

        # Make sure both either start at 0 or finish at 1!
        self.assert_ordering(self.coords_bndry[:,0], ind_sort_mesh, x_dof_Wc, ind_sort_Wc, "mesh")

        for (i,ind) in enumerate(ind_sort_mesh):
            theta.dat.data[ind_sort_Wc[i]] = self.coords_bndry[ind][1]

        return theta

    def assert_ordering(self, x_U, ind_sort_U, x_Uc, ind_sort_Uc, name):
        x0_U = x_U[ind_sort_U[0]]
        x0_Uc = x_Uc[ind_sort_Uc[0]]
        print( ("dof " + name + " start at %.4f, dof " + name + "_c start at %.4f") % (x0_U, x0_Uc))
        assert x0_U == x0_Uc, "different numbering for mesh and boundary mesh, name"

    def save_file(self):
        u, p = self.z.split()
        self.uplot.assign(u)
        self.pplot.assign(p)
        self.File_u.write(self.uplot)
        self.File_p.write(self.pplot)



if __name__ == "__main__":

    parser = get_default_parser()
    parser.add_argument("--A", type=float, default="0.5")
    parser.add_argument("--n", type=float, default="1")
    parser.add_argument("--L", type=float, default="1")
    parser.add_argument("--utop", type=float, default="1")
    parser.add_argument("--N", type=float, default="1")
    parser.add_argument("--amp", type=float, default="0.05")
    args, _ = parser.parse_known_args()

    # Domain
    H = args.L

    # Viscosity
    ng_val = Constant(args.n)
    eps_min_val =  Constant(1.0e-2)
    A_val = Constant(args.A)

    # Forcing terms
    N = Constant(args.N)

    # Initialise problem
    problem = SubGlacialCavity(N, args.L, H, args.amp, args.utop, A_val, ng_val, eps_min_val, args)

    # Uzawa algorithm
    tol = 1e-5
    c_Uzawa = 0.1
    k_max = 2000
    iter = UzawaSolver(problem, tol, c_Uzawa, k_max)

    # Plot
    lamda = problem.lamda

    # Plot
    import matplotlib.pylab as plt
    colors = ["red", "blue", "green"]
    def plotandsave(x,y,file, legend = None):
        fig = plt.figure()
        for i in range(len(x)): plt.plot(x[i], y[i], color = colors[i])
        if legend is not None: plt.legend(legend)
        plt.savefig(file)
        plt.close(fig)

    # Plot lamda
    xM_sort, dofM_sort = SortedXDoF(problem.M, 1)
    plotandsave([xM_sort], [lamda.dat.data[dofM_sort]], "output/lamda.png")

    # Check contact conditions
    def plus(x):
        return conditional(ge(x, 0), x, 0)

    n = FacetNormal(problem.mesh)
    u = problem.z.split()[0]
    p = problem.z.split()[1]
    penalty_u_n = plus(dot(u,n))**2 * ds(1)
    tracn = physics.traction(p,u,u,n,eps_min_val,A_val,ng_val,problem.Z.sub(0))
    penalty_complementarity = (dot(u,n) * dot(tracn,n))**2 * ds(1)
    penalty_sigma_nn = plus(dot(tracn, n))**2 * ds(1)
    print("VALUE OF || (u_n)+ ||^2_b :: %.2e" % assemble(penalty_u_n))
    print("VALUE OF || (sigma_nn)+ ||^2_b :: %.2e" % assemble(penalty_sigma_nn))
    print("VALUE OF un . sigma_nn :: %.2e" % assemble(penalty_complementarity))
