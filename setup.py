from setuptools import setup

setup(name="OPS",
      version="0.0.1",
      description="(O)bstacle (P)roblem (S)olvers",
      packages = ["ops"])
