from firedrake import *

class ObstacleProblem(object):

    def solver(self, V):
        raise NotImplementedError

    def complementarity(self, c):
        raise NotImplementedError

    def dual_diff(self, lamda, mu):
        raise NotImplementedError

    def update(self, mu):
        raise NotImplementedError

    def save_file(self):
        pass
