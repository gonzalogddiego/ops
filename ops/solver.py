from firedrake import *
from .mlogging import *
import numpy as np

def UzawaSolver(problem, tol, c_Uzawa, k_max = 100):

    info_green("UZAWA SOLVER")
    k = 1

    solver = problem.primal_solver()

    while True:

        info_blue("Iteration = %d" % float(k))

        # Solve for u
        solver.solve()
        problem.save_file()

        # Update lamda
        F = problem.complementarity(c_Uzawa)
        lamda_new = problem.construct_lamda_new(F)

        # import pdb; pdb.set_trace()

        # Measure difference
        diff = problem.dual_error(lamda_new)
        info_blue("  ||lambda_k - lambda_k-1|| = %.2e" % diff)

        if diff < tol:
            info_red("Terminating because diff < tol")
            break

        if k == k_max:
            info_red("Terminating because k = k_max")
            break

        problem.update(lamda_new) # Update lamda
        k += 1

    return k


def PrimalDualActiveSet(problem, tol, c_pdas, k_max = 20):

    info_green("PRIMAL DUAL ACTIVE SET SOLVER")
    k = 1

    solver = problem.dual_solver()

    while True:

        info_blue("Iteration = %d" % float(k))

        problem.lamda_old.assign(problem.lamda)

        # Set bcs accordingly: find active and inactive sets
        F = problem.complementarity(c_pdas)
        A = np.where(F > 0)
        problem.update_cutoff_fcns(A)

        # Solve
        solver.solve()
        problem.dual_update()
        problem.save_file()

        # Measure difference
        diff = problem.dual_error(problem.lamda_old)
        info_blue("  ||lambda_k - lambda_k-1|| = %.2e" % diff)

        # import pdb; pdb.set_trace()

        if diff < tol:
            info_red("Terminating because diff < tol")
            break

        if k == k_max:
            info_red("Terminating because k = k_max")
            break

        k += 1

    return k
