from firedrake import *
import numpy as np

class DirichletBCDoFs(DirichletBC):
    def __init__(self,indices_dofs, V, g):
        super().__init__(V, g, None)
        self.indices_dofs = indices_dofs
    @utils.cached_property
    def nodes(self):
        return self.indices_dofs

def DoFIndex(U, label):
    entity_dofs = U.finat_element.entity_closure_dofs()
    mesh = U.mesh()
    indices = mesh.measure_set("exterior_facet", label).indices
    dofs = U.exterior_facet_node_map().values[indices, :]
    facet_no = mesh.exterior_facets.local_facet_dat.data_ro[indices]
    label_dofs = []
    for row, facet in zip(dofs, facet_no):
        label_dofs.extend(row[entity_dofs[1][facet]])
    seen = set()
    return [x for x in label_dofs if not (x in seen or seen.add(x))]

def DoFCoords(U):
    mesh = U.mesh()
    U_ufl = U.ufl_element()
    coords_DoF = interpolate(SpatialCoordinate(mesh), VectorFunctionSpace(mesh, U_ufl.family(), U_ufl.degree()))
    return coords_DoF

def DoFCoordsLabel(U, label):
    coords_DoF = DoFCoords(U)
    DoFU_label = DoFIndex(U, label)
    return coords_DoF.dat.data[DoFU_label, :]

def SortedXDoF(U, label):
    mesh = U.mesh()
    dofU_contact = DoFIndex(U, label)
    coords_dofU_contact = DoFCoordsLabel(U, label)
    ind_sort_U_contact = np.argsort(coords_dofU_contact[:,0])
    coordsX_dofU_contact_sorted = np.sort(coords_dofU_contact[:,0])
    dofU_contact_sorted = [x for _,x in sorted(zip(ind_sort_U_contact, dofU_contact))]
    return coordsX_dofU_contact_sorted, dofU_contact_sorted
